/**
  version.go: 版本管理
 */
package version

var (
	//git 版本
	Version   = "3.0.1"
	//git 提交头
	GitCommit = "HEAD"
)
